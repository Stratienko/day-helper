import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./Reducers";

export default (initialState: any) => {
  const Store = createStore(rootReducer, initialState, applyMiddleware(thunk));

  return Store;
};
