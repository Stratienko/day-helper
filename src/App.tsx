import { createBrowserHistory } from "history";
import * as React from "react";
import { Provider } from "react-redux";
import { Router } from "react-router";
import store from "./Redux/index";
import theme from "./theme.css";

const Store = store({});

const browserHistory = createBrowserHistory();

class App extends React.Component {
  render() {
    return (
      <Provider store={Store}>
        <Router history={browserHistory}>
          <div className={theme.app}>Hello, Electron!</div>
        </Router>
      </Provider>
    );
  }
}

export default App;
